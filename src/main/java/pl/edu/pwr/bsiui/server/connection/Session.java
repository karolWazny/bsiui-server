package pl.edu.pwr.bsiui.server.connection;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;
import pl.edu.pwr.bsiui.server.business.model.Principal;

import javax.crypto.SecretKey;
import java.security.PublicKey;

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
class Session {

    static ThreadLocal<Session> instance = ThreadLocal.withInitial(Session::new);

    static Session getSession(){
        return instance.get();
    }

    private Session(){}

    Principal principal = Principal.anonymous();
    PublicKey publicKey;
    SecretKey aesKey;
}
