package pl.edu.pwr.bsiui.server.business.messaging;

import pl.edu.pwr.bsiui.server.business.model.ConversationUpdate;
import pl.edu.pwr.bsiui.server.business.model.Message;
import pl.edu.pwr.bsiui.server.business.model.SentMessage;

import java.util.List;

public interface Messaging {
    SentMessage send(Message message);
    List<ConversationUpdate> getUserNewMessages(String username);
    List<SentMessage> getConversationMessages(String conversationName, String currentUserLogin);
}
