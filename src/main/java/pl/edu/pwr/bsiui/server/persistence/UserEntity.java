package pl.edu.pwr.bsiui.server.persistence;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@Table(schema = "bsiui", name = "USERS")
class UserEntity {
    @Id
    @SequenceGenerator(name = "USERS_GENERATOR", sequenceName = "bsiui.USERS_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "USERS_GENERATOR")
    Long id;

    @Column(unique = true)
    String login;

    String password;

    @OneToMany(mappedBy = "user")
    List<MembershipEntity> memberships;
}
