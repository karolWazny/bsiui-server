package pl.edu.pwr.bsiui.server.persistence;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@Table(schema = "bsiui", name = "MESSAGES")
class MessageEntity {
    @Id
    @SequenceGenerator(name = "MESSAGES_GENERATOR", sequenceName = "bsiui.MESSAGES_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "MESSAGES_GENERATOR")
    Long id;

    @JoinColumn(name = "CONVERSATION_ID")
    @ManyToOne
    ConversationEntity conversation;

    @JoinColumn(name = "AUTHOR_ID")
    @ManyToOne
    UserEntity userEntity;

    OffsetDateTime timeSent;

    String content;
}
