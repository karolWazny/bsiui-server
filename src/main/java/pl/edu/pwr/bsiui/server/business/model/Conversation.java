package pl.edu.pwr.bsiui.server.business.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.Set;

@Builder
@Getter
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@ToString
public class Conversation {
    String name;
    Set<String> users;
}
