package pl.edu.pwr.bsiui.server.business.persistence;

import pl.edu.pwr.bsiui.server.business.model.User;

import java.util.Set;

public interface Users {
    boolean usernameExists(String username);
    User add(User user);
    Set<String> all();
    boolean contain(User user);
}
