package pl.edu.pwr.bsiui.server.persistence;

import org.mapstruct.*;
import pl.edu.pwr.bsiui.server.business.model.Conversation;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
interface ConversationMapper {
    @Mapping(source = "memberships", target = "users")
    Conversation conversationEntityToConversation(ConversationEntity conversationEntity);

    default Set<String> membershipsToUsers(List<MembershipEntity> membershipEntityList){
        if (membershipEntityList == null)
            return null;
        return membershipEntityList.stream()
                .map(this::membershipToUserName)
                .collect(Collectors.toSet());
    }

    default String membershipToUserName(MembershipEntity membershipEntity) {
        return membershipEntity.getUser().getLogin();
    }
}
