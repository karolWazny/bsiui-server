package pl.edu.pwr.bsiui.server.business.usermanagement;

import pl.edu.pwr.bsiui.server.business.model.User;

import java.util.Set;

public interface UserManagement {
    String registerUser(User user);

    Set<String> listUsers();
}
