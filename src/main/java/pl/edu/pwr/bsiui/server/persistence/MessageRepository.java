package pl.edu.pwr.bsiui.server.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.OffsetDateTime;
import java.util.List;

@Repository
interface MessageRepository extends JpaRepository<MessageEntity, Long> {
    List<MessageEntity> findAllByConversationNameOrderByTimeSentDesc(String conversationName);
    List<MessageEntity> findAllByConversationNameAndTimeSentGreaterThanOrderByTimeSentDesc(String conversationName, OffsetDateTime since);
}
