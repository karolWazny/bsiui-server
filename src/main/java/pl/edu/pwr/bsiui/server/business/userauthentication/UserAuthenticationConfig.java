package pl.edu.pwr.bsiui.server.business.userauthentication;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.edu.pwr.bsiui.server.business.persistence.Users;

@Configuration
class UserAuthenticationConfig {
    @Bean
    public UserAuthentication userAuthentication(Users users) {
        return new SimpleUserAuthentication(users);
    }
}
