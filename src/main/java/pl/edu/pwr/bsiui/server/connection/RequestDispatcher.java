package pl.edu.pwr.bsiui.server.connection;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiFunction;
import java.util.function.Function;

public class RequestDispatcher {

    private final Map<String, RequestHandler> actionWithRequestClass = new ConcurrentHashMap<>();

    <T, U> void registerRequestHandler(String handlerName,
                                    T serviceBean,
                                    Class<U> businessModelRequestClass,
                                    BiFunction<? super T, U, ?> handlerMethod){
        this.actionWithRequestClass.put(handlerName,
                new RequestHandler<>(businessModelRequestClass,
                        serviceBean,
                        handlerMethod));
    }
    <T, U> void registerRequestHandler(String handlerName,
                                    T serviceBean,
                                    Class<U> businessModelRequestClass,
                                    BiFunction<? super T, U, ?> handlerMethod,
                                    Function<? super RequestHandler<T, U>, Void> intermediateProcess,
                                    Function<? super RequestHandler<T, U>, Void> postProcess){
        this.actionWithRequestClass.put(handlerName,
                new RequestHandler<>(businessModelRequestClass,
                        serviceBean,
                        handlerMethod,
                        intermediateProcess,
                        postProcess));
    }

    String routeAndExecute(PreprocessedRequest req) {
        String type = req.requestType;
        RequestHandler requestHandler = this.actionWithRequestClass.get(type);
        return requestHandler.handleRequest(req.jsonBody);
    }
}


class AuthenticationException extends RuntimeException {}