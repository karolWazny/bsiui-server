package pl.edu.pwr.bsiui.server.business.userauthentication;

import lombok.RequiredArgsConstructor;
import pl.edu.pwr.bsiui.server.business.model.Principal;
import pl.edu.pwr.bsiui.server.business.model.User;
import pl.edu.pwr.bsiui.server.business.persistence.Users;
import pl.edu.pwr.bsiui.server.utils.Hasher;

@RequiredArgsConstructor
class SimpleUserAuthentication implements UserAuthentication {

    private final Users users;

    @Override
    public Principal authenticateUser(Principal unauthenticated) {
        String passwordHash = Hasher.hash(unauthenticated.getPassword());
        User userExample = new User(unauthenticated.getLogin(), passwordHash);
        if (users.contain(userExample)) {
            return Principal.builder()
                    .login(unauthenticated.getLogin())
                    .password("")
                    .isAuthenticated(true)
                    .build();
        }
        return unauthenticated;
    }
}
