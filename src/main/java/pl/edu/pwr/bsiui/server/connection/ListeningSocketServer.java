package pl.edu.pwr.bsiui.server.connection;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;

public class ListeningSocketServer extends Thread {
    private ServerSocket serverSocket;
    private final int port;
    private final RequestDispatcher requestDispatcher;

    public ListeningSocketServer(int port, RequestDispatcher requestDispatcher) {
        this.port = port;
        this.requestDispatcher = requestDispatcher;
    }

    public void run() {
        try {
            serverSocket = new ServerSocket(this.port);
            while (true) {
                var connection = serverSocket.accept();

                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                PrintWriter out = new PrintWriter(connection.getOutputStream(), true);

                // HI
                String hiMessage = in.readLine();
                if (hiMessage.equals("Hi!")) {

                    var clientHandler = new ClientSocketHandler(0, requestDispatcher);
                    clientHandler.start();
                    int port = clientHandler.getListeningPort();
                    out.println(port);
                    out.close();
                }

            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            terminate();
        }
    }

    public void terminate() {
        try {
            serverSocket.close();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
