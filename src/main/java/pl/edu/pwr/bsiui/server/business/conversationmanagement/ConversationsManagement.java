package pl.edu.pwr.bsiui.server.business.conversationmanagement;

import pl.edu.pwr.bsiui.server.business.model.Conversation;

import java.util.Set;

public interface ConversationsManagement {
    Conversation createConversation(Conversation conversation);
    Set<Conversation> ofUser(String username);
    Set<Conversation> allConversations();
}
