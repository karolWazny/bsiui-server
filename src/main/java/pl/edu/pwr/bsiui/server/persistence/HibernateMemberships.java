package pl.edu.pwr.bsiui.server.persistence;

import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.pwr.bsiui.server.business.model.Membership;
import pl.edu.pwr.bsiui.server.business.persistence.Memberships;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.List;

@RequiredArgsConstructor
class HibernateMemberships implements Memberships {

    private final MembershipsRepository membershipsRepository;
    private final MembershipMapper membershipMapper;
    private final DateTimeMapper dateTimeMapper;
    private final UserRepository userRepository;
    private final ConversationsRepository conversationsRepository;

    @Override
    @Transactional
    public void setLastSeen(String currentUserLogin, String conversationName, LocalDateTime timeSeen) {
        membershipsRepository.findMembershipEntityByUserLoginAndConversationName(currentUserLogin,
                conversationName)
                .ifPresent(membershipEntity -> {
                    OffsetDateTime offsetDateTime = dateTimeMapper.localToOffset(timeSeen);
                    membershipEntity.setLastSeen(offsetDateTime);
                    membershipsRepository.save(membershipEntity);
                });
    }

    @Override
    public List<Membership> of(String username) {
        return membershipsRepository.findMembershipEntitiesByUserLogin(username)
                .stream()
                .map(membershipMapper::membershipEntityToMembership)
                .toList();
    }

    @Override
    @Transactional
    public void addUserTo(String username, String conversationName) {
        UserEntity user = userRepository.findUserEntityByLogin(username);
        ConversationEntity conversation = conversationsRepository.findConversationEntityByName(conversationName);
        MembershipEntity membership = MembershipEntity.builder()
                .lastSeen(dateTimeMapper.localToOffset(LocalDateTime.now()))
                .conversation(conversation)
                .user(user)
                .id(new MembershipEntity.MembershipId(user.getId(), conversation.getId()))
                .build();
        membershipsRepository.save(membership);
    }
}
