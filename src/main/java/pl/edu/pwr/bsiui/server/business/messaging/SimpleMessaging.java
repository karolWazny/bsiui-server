package pl.edu.pwr.bsiui.server.business.messaging;

import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.pwr.bsiui.server.business.model.ConversationUpdate;
import pl.edu.pwr.bsiui.server.business.model.Message;
import pl.edu.pwr.bsiui.server.business.model.SentMessage;
import pl.edu.pwr.bsiui.server.business.persistence.Memberships;
import pl.edu.pwr.bsiui.server.business.persistence.Messages;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

@RequiredArgsConstructor
class SimpleMessaging implements Messaging {

    private final Messages messages;
    private final Memberships memberships;

    @Override
    @Transactional
    public SentMessage send(Message message) {
        return messages.add(message);
    }

    @Override
    @Transactional
    public List<ConversationUpdate> getUserNewMessages(String username) {
        return memberships.of(username).stream()
                .map(membership -> {
                    List<SentMessage> messagesInConversation = messages.inConversationSince(membership.getConversationName(), membership.getLastSeen());
                    if (messagesInConversation.isEmpty())
                        return null;
                    memberships.setLastSeen(username, membership.getConversationName(), LocalDateTime.now());
                    return ConversationUpdate.builder()
                            .conversation(membership.getConversationName())
                            .messages(messagesInConversation)
                            .build();
                })
                .filter(Objects::nonNull)
                .toList();
    }

    @Override
    @Transactional
    public List<SentMessage> getConversationMessages(String conversationName, String currentUserLogin) {
        List<SentMessage> result = messages.inConversation(conversationName);
        memberships.setLastSeen(currentUserLogin, conversationName, LocalDateTime.now());
        return result;
    }
}
