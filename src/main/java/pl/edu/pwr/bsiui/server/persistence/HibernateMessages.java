package pl.edu.pwr.bsiui.server.persistence;

import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.pwr.bsiui.server.business.model.Message;
import pl.edu.pwr.bsiui.server.business.model.SentMessage;
import pl.edu.pwr.bsiui.server.business.persistence.Messages;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
class HibernateMessages implements Messages {

    private final MessageRepository messageRepository;
    private final MessageMapper messageMapper;
    private final DateTimeMapper dateTimeMapper;
    private final ConversationsRepository conversationsRepository;
    private final UserRepository userRepository;

    @Override
    @Transactional
    public SentMessage add(Message message) {
        ConversationEntity conversation = conversationsRepository.findConversationEntityByName(message.getConversation());
        UserEntity author = userRepository.findUserEntityByLogin(message.getAuthor());
        MessageEntity messageEntity = MessageEntity.builder()
                .content(message.getContent())
                .conversation(conversation)
                .userEntity(author)
                .timeSent(dateTimeMapper.localToOffset(LocalDateTime.now()))
                .build();
        return messageMapper.messageEntityToSentMessage(messageRepository.save(messageEntity));
    }

    @Override
    public List<SentMessage> inConversation(String conversationName) {
        return messageRepository.findAllByConversationNameOrderByTimeSentDesc(conversationName)
                .stream()
                .map(messageMapper::messageEntityToSentMessage)
                .toList();
    }

    @Override
    public List<SentMessage> inConversationSince(String conversationName, LocalDateTime lastSeen) {
        return messageRepository.findAllByConversationNameAndTimeSentGreaterThanOrderByTimeSentDesc(
                        conversationName,
                        dateTimeMapper.localToOffset(lastSeen)
                ).stream()
                .map(messageMapper::messageEntityToSentMessage)
                .collect(Collectors.toList());
    }
}
