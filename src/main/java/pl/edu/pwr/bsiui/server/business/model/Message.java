package pl.edu.pwr.bsiui.server.business.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Builder
@Getter
@Setter
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@ToString
public class Message {
    String author;
    String content;
    String conversation;
}
