package pl.edu.pwr.bsiui.server.dto.requests;

public class SendMessageDto {
    public String conversation;
    public String content;
}
