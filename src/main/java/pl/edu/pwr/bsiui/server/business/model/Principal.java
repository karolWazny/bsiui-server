package pl.edu.pwr.bsiui.server.business.model;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

@Getter
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Principal {

    public static Principal anonymous() {
        return Principal.builder()
                .login("anonymous")
                .password("")
                .isAuthenticated(false)
                .build();
    }


    @Setter
    String login;
    @Setter
    String password;

    boolean isAuthenticated;
}
