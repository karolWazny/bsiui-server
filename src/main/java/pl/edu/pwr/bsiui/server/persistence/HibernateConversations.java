package pl.edu.pwr.bsiui.server.persistence;

import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.pwr.bsiui.server.business.model.Conversation;
import pl.edu.pwr.bsiui.server.business.persistence.Conversations;

import java.util.Set;
import java.util.stream.Collectors;

@RequiredArgsConstructor
class HibernateConversations implements Conversations {

    private final ConversationsRepository conversationsRepository;
    private final ConversationMapper conversationMapper;

    @Override
    @Transactional
    public Set<Conversation> all() {
        return conversationsRepository.findAll()
                .stream()
                .map(conversationMapper::conversationEntityToConversation)
                .collect(Collectors.toSet());
    }

    @Override
    public void addNamed(String name) {
        ConversationEntity entity = ConversationEntity.builder()
                .name(name)
                .build();
        conversationsRepository.save(entity);
    }

    @Override
    @Transactional
    public Set<Conversation> of(String username) {
        return conversationsRepository.findConversationsWithUser(username)
                .stream()
                .map(conversationMapper::conversationEntityToConversation)
                .collect(Collectors.toSet());
    }
}
