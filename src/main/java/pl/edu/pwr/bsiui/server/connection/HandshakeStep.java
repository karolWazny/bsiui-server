package pl.edu.pwr.bsiui.server.connection;

public enum HandshakeStep {
    INITIALIZATION,
    RSA_KEY_EXCHANGE,
    VERIFICATION,
    AES_KEY_EXCHANGE,
    CONVERSATION,
}
