package pl.edu.pwr.bsiui.server.business.messaging;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.edu.pwr.bsiui.server.business.persistence.Memberships;
import pl.edu.pwr.bsiui.server.business.persistence.Messages;

@Configuration
class MessagingConfig {
    @Bean
    public Messaging messaging(Messages messages,
                               Memberships memberships) {
        return new SimpleMessaging(messages, memberships);
    }
}
