package pl.edu.pwr.bsiui.server.business.usermanagement;

import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.pwr.bsiui.server.business.model.User;
import pl.edu.pwr.bsiui.server.business.persistence.Users;
import pl.edu.pwr.bsiui.server.utils.Hasher;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Set;

@RequiredArgsConstructor
class SimpleUserManagement implements UserManagement {
    private final Users users;

    @Override
    public String registerUser(User user) {
        try {
            if (users.usernameExists(user.getLogin()))
                throw new RuntimeException();
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            String passwordHash = Hasher.hash(user.getPassword());
            User userToSave = User.builder()
                    .login(user.getLogin())
                    .password(passwordHash)
                    .build();
            users.add(userToSave);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return user.getLogin();
    }

    @Override
    public Set<String> listUsers() {
        return users.all();
    }
}
