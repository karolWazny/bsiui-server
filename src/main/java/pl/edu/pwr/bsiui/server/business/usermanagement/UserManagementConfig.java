package pl.edu.pwr.bsiui.server.business.usermanagement;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.edu.pwr.bsiui.server.business.persistence.Users;

@Configuration
class UserManagementConfig {
    @Bean
    public UserManagement userManagement(Users users) {
        return new SimpleUserManagement(users);
    }
}
