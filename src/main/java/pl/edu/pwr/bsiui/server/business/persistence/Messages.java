package pl.edu.pwr.bsiui.server.business.persistence;

import pl.edu.pwr.bsiui.server.business.model.Message;
import pl.edu.pwr.bsiui.server.business.model.SentMessage;

import java.time.LocalDateTime;
import java.util.List;

public interface Messages {
    SentMessage add(Message message);

    List<SentMessage> inConversation(String conversationName);

    List<SentMessage> inConversationSince(String conversationName, LocalDateTime lastSeen);
}
