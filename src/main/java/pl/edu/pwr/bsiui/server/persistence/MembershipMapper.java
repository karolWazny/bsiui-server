package pl.edu.pwr.bsiui.server.persistence;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import pl.edu.pwr.bsiui.server.business.model.Membership;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE,
        uses = DateTimeMapper.class,
        componentModel = "spring",
        injectionStrategy = InjectionStrategy.CONSTRUCTOR)
interface MembershipMapper {

    @Mapping(source = "user.login", target = "username")
    @Mapping(source = "conversation.name", target = "conversationName")
    Membership membershipEntityToMembership(MembershipEntity membershipEntity);
}
