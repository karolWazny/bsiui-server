package pl.edu.pwr.bsiui.server.business.persistence;

import pl.edu.pwr.bsiui.server.business.model.Membership;

import java.time.LocalDateTime;
import java.util.List;

public interface Memberships {
    void setLastSeen(String currentUserLogin, String conversationName, LocalDateTime timeSeen);

    List<Membership> of(String username);

    void addUserTo(String username, String conversationName);
}
