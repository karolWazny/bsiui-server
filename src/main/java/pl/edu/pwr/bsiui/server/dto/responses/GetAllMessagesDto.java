package pl.edu.pwr.bsiui.server.dto.responses;

import pl.edu.pwr.bsiui.server.dto.Message;

public class GetAllMessagesDto {
    public ConversationWithMessages[] updatedConversations;
}

class ConversationWithMessages {
    public String conversation;
    public Message[] messages;
}