package pl.edu.pwr.bsiui.server.connection;

import com.google.gson.Gson;

import java.util.function.BiFunction;
import java.util.function.Function;

public class RequestHandler<T, U> {
    private final Function<? super RequestHandler<T, U>, Void> postProcess;
    Class<U> businessModelRequestClass;
    T serviceBean;
    BiFunction<? super T, U, ?> method;
    Function<? super RequestHandler<T, U>, Void> preProcess;

    ThreadLocal<U> argument = ThreadLocal.withInitial(() -> null);
    ThreadLocal<Object> result = ThreadLocal.withInitial(() -> null);

    RequestHandler(Class<U> businessModelRequestClass,
                   T serviceBean,
                   BiFunction<? super T, U, ?> method) {
        this(businessModelRequestClass,
                serviceBean,
                method,
                (handler) -> null,
                (handler) -> null);
    }

    RequestHandler(Class<U> businessModelRequestClass,
                   T serviceBean,
                   BiFunction<? super T, U, ?> method,
                   Function<? super RequestHandler<T, U>, Void> preprocess,
                   Function<? super RequestHandler<T, U>, Void> postProcess) {
        this.businessModelRequestClass = businessModelRequestClass;
        this.serviceBean = serviceBean;
        this.method = method;
        this.preProcess = preprocess;
        this.postProcess = postProcess;
    }

    public Void checkAuthenticated() {
        if (!Session.getSession().getPrincipal().isAuthenticated()) {
            throw new AuthenticationException();
        }
        return null;
    }

    public Void noOp() {
        return null;
    }

    String handleRequest(String jsonBody) {
        result.remove();
        argument.set((new Gson()).fromJson(jsonBody, this.businessModelRequestClass));
        try {
            preProcess.apply(this);
            result.set(this.method.apply(serviceBean, argument.get()));
            postProcess.apply(this);
            return new Gson().toJson(result.get());
        } catch (AuthenticationException e) {
            return "{\"status\":401}";
        } catch (Exception e) {
            return "{\"status\":500}";
        }
    }
}
