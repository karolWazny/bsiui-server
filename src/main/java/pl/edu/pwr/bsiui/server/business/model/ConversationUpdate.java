package pl.edu.pwr.bsiui.server.business.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Builder
@Getter
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@ToString
public class ConversationUpdate {
    String conversation;
    List<SentMessage> messages;
}
