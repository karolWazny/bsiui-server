package pl.edu.pwr.bsiui.server.connection;

import com.google.common.hash.Hashing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.edu.pwr.bsiui.server.ciphering.AES;
import pl.edu.pwr.bsiui.server.ciphering.Ciphering;
import pl.edu.pwr.bsiui.server.ciphering.ServerKeyStore;

import javax.crypto.SecretKey;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

public class ClientSocketHandler extends Thread {
    private ServerSocket serverSocket;
    private PrintWriter out;
    private BufferedReader in;
    private RequestDispatcher router;
    private HandshakeStep handshakeStep;
    private PublicKey clientPublic;
    private int port;

    private Logger LOGGER = LoggerFactory.getLogger(ClientSocketHandler.class);

    public ClientSocketHandler(int port, RequestDispatcher requestDispatcher) {
        try {
            this.port = port;
            this.serverSocket = new ServerSocket(0);
            this.router = requestDispatcher;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getListeningPort() {
        return this.serverSocket.getLocalPort();
    }

    public void run() {

        try {
            Socket clientSocket = this.serverSocket.accept();
            this.handshakeStep = HandshakeStep.INITIALIZATION;

            out = new PrintWriter(clientSocket.getOutputStream(), true);
            in = new BufferedReader(
                    new InputStreamReader(clientSocket.getInputStream()));

            String inputLine;

            this.handshakeStep = HandshakeStep.RSA_KEY_EXCHANGE;
            while ((inputLine = in.readLine()) != null) {
                String response = null;

                if (this.handshakeStep != HandshakeStep.CONVERSATION) {
                    response = this.handleHandshake(inputLine);
                } else {

                    // get key
                    SecretKey aes = Session.getSession().getAesKey();
                    String encodedKey = Base64.getEncoder().encodeToString(aes.getEncoded());

                    // decrypt and preprocess
                    String decryptedMessage = AES.decrypt(inputLine, encodedKey);
                    System.out.println("DECRYPTED REQUEST: " + decryptedMessage);
                    PreprocessedRequest rawRequest = new PreprocessedRequest(decryptedMessage);

                    // get response and encrypt
                    String rawResponse = this.router.routeAndExecute(rawRequest);
                    System.out.println("RAW RESPONSE: " + rawResponse);
                    String encryptedResponse = AES.encrypt(rawResponse, encodedKey);

                    response = encryptedResponse;
                }

                out.println(response);
            }
            in.close();
            out.close();
            serverSocket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String handleHandshake(String request) {
        switch (this.handshakeStep) {
            case RSA_KEY_EXCHANGE:
                try {
                    this.handshakeStep = HandshakeStep.VERIFICATION;
                    // recieve client public key
                    KeyFactory keyFactory = KeyFactory.getInstance("RSA");
                    X509EncodedKeySpec keySpecX509 = new X509EncodedKeySpec(Base64.getDecoder().decode(request));
                    this.clientPublic = keyFactory.generatePublic(keySpecX509);
                    LOGGER.info("SERVER RECIEVED CLIENT PUBLIC : " + this.clientPublic);

                    // send ciphered server public
                    PublicKey serverPub = ServerKeyStore.getInstance().getServerPublic();

                    String cipheredServerPublic = Ciphering.encryptHugeText(Base64.getEncoder().encodeToString(serverPub.getEncoded()), this.clientPublic);
                    LOGGER.info("SERVER PUBLIC : " + serverPub);
                    return cipheredServerPublic;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                break;
            case VERIFICATION:
                // recieve and decipher authorization message
                try {
                    this.handshakeStep = HandshakeStep.AES_KEY_EXCHANGE;
                    PrivateKey serverPriv = ServerKeyStore.getInstance().getServerPrivate();
                    String decryptedValidation = Ciphering.decryptHugeText(request, serverPriv);
                    LOGGER.info("SERVER RECIEVED VALIDATION: " + decryptedValidation);

                    String controlHashTest = Hashing
                            .sha256()
                            .hashString(decryptedValidation + Base64.getEncoder().encodeToString(this.clientPublic.getEncoded()), StandardCharsets.UTF_8)
                            .toString();

                    String encryptedResponse = Ciphering.encryptHugeText(controlHashTest, this.clientPublic);

                    LOGGER.info("SERVER SENDS HASHED VALIDATION: " + encryptedResponse);
                    return encryptedResponse;

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                break;
            case AES_KEY_EXCHANGE:
                // recieve aes key
                try {
                    this.handshakeStep = HandshakeStep.CONVERSATION;
                    PrivateKey serverPriv = ServerKeyStore.getInstance().getServerPrivate();
                    String decryptedAES = Ciphering.decryptHugeText(request, serverPriv);
                    Session.getSession().setAesKey(AES.secretKeyFromString(decryptedAES));
                    LOGGER.info("SERVER RECIEVED AES: " + decryptedAES);

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                break;
        }
        return null;
    }
}