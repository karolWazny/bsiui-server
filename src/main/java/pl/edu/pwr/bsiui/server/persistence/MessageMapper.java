package pl.edu.pwr.bsiui.server.persistence;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import pl.edu.pwr.bsiui.server.business.model.SentMessage;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE,
        uses = DateTimeMapper.class,
        componentModel = "spring",
        injectionStrategy = InjectionStrategy.CONSTRUCTOR)
interface MessageMapper {
    @Mapping(source = "userEntity.login", target = "author")
    SentMessage messageEntityToSentMessage(MessageEntity message);
}
