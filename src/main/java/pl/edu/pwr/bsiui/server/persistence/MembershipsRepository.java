package pl.edu.pwr.bsiui.server.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.Set;

@Repository
interface MembershipsRepository extends JpaRepository<MembershipEntity, MembershipEntity.MembershipId> {
    Optional<MembershipEntity> findMembershipEntityByUserLoginAndConversationName(String login, String conversationName);
    Set<MembershipEntity> findMembershipEntitiesByUserLogin(String login);
}
