package pl.edu.pwr.bsiui.server.business.conversationmanagement;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.edu.pwr.bsiui.server.business.persistence.Conversations;
import pl.edu.pwr.bsiui.server.business.persistence.Memberships;

@Configuration
class ConversationManagementConfig {
    @Bean
    public ConversationsManagement conversationsManagement(Conversations conversations,
                                                           Memberships memberships) {
        return new SimpleConversationsManagement(conversations, memberships);
    }
}
