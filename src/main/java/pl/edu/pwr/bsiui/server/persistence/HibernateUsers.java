package pl.edu.pwr.bsiui.server.persistence;

import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.pwr.bsiui.server.business.model.User;
import pl.edu.pwr.bsiui.server.business.persistence.Users;

import java.util.Set;
import java.util.stream.Collectors;

@RequiredArgsConstructor
class HibernateUsers implements Users {
    private final UserRepository userRepository;
    private final UserMapper userMapper;

    @Override
    public boolean usernameExists(String username) {
        return userRepository.existsUserEntityByLogin(username);
    }

    @Override
    @Transactional
    public User add(User user) {
        UserEntity userEntity = userMapper.userToUserEntity(user);
        UserEntity savedUserEntity = userRepository.save(userEntity);
        return userMapper.entityToUser(savedUserEntity);
    }

    @Override
    public Set<String> all() {
        return userRepository.findAll()
                .stream()
                .map(UserEntity::getLogin)
                .collect(Collectors.toSet());
    }

    @Override
    public boolean contain(User user) {
        return userRepository.existsUserEntityByLoginAndPassword(user.getLogin(), user.getPassword());
    }
}
