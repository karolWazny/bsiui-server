package pl.edu.pwr.bsiui.server.connection;

import com.google.gson.JsonParser;

public class PreprocessedRequest {
    private String message;
    public String requestType;
    public String jsonBody;

    PreprocessedRequest(String message) {
        this.message = message;
        this.requestType = this.getRequestType();
        this.jsonBody = this.getRequestBodyJSON();
    }

    private String getRequestType() {

        var val = JsonParser.parseString(this.message)
                .getAsJsonObject()
                .get("action")
                .getAsString();

        return val;
    }

    private String getRequestBodyJSON() {
        var val = JsonParser.parseString(this.message)
                .getAsJsonObject()
                .get("body")
                .toString();

        return val;
    }
}
