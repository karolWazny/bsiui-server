package pl.edu.pwr.bsiui.server.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
interface ConversationsRepository extends JpaRepository<ConversationEntity, Long> {
    @Query("from ConversationEntity conversation join conversation.memberships m where m.user.login = :username")
    Set<ConversationEntity> findConversationsWithUser(String username);
    ConversationEntity findConversationEntityByName(String conversationName);
}
