package pl.edu.pwr.bsiui.server.dto.requests;

public class CreateConversationDto {
    public String name;
    public String[] users;
}
