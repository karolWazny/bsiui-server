package pl.edu.pwr.bsiui.server.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
interface UserRepository extends JpaRepository<UserEntity, Long> {
    Boolean existsUserEntityByLogin(String login);
    Boolean existsUserEntityByLoginAndPassword(String login, String password);
    UserEntity findUserEntityByLogin(String login);
}
