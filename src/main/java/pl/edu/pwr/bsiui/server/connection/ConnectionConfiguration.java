package pl.edu.pwr.bsiui.server.connection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.PropertySource;
import pl.edu.pwr.bsiui.server.business.conversationmanagement.ConversationsManagement;
import pl.edu.pwr.bsiui.server.business.messaging.Messaging;
import pl.edu.pwr.bsiui.server.business.model.Conversation;
import pl.edu.pwr.bsiui.server.business.model.Message;
import pl.edu.pwr.bsiui.server.business.model.Principal;
import pl.edu.pwr.bsiui.server.dto.EmptyDto;
import pl.edu.pwr.bsiui.server.business.model.User;
import pl.edu.pwr.bsiui.server.business.userauthentication.UserAuthentication;
import pl.edu.pwr.bsiui.server.business.usermanagement.UserManagement;
import pl.edu.pwr.bsiui.server.dto.requests.GetMessagesDto;

@Lazy
@Configuration
@PropertySource("classpath:bsiui.properties")
public class ConnectionConfiguration {

    @Value("${bsiui.server.port}")
    private Integer port;

    @Autowired
    UserManagement userManagement;

    @Autowired
    UserAuthentication userAuthentication;

    @Autowired
    Messaging messaging;

    @Autowired
    ConversationsManagement conversationsManagement;

    @Bean
    RequestDispatcher requestDispatcher() {
        RequestDispatcher requestDispatcher = new RequestDispatcher();
        requestDispatcher.registerRequestHandler("register",
                userManagement,
                User.class,
                UserManagement::registerUser,
                RequestHandler::checkAuthenticated,
                RequestHandler::noOp);
        requestDispatcher.registerRequestHandler("listUsers",
                userManagement,
                EmptyDto.class,
                (userManagement, emptyDto)->userManagement.listUsers(),
                RequestHandler::checkAuthenticated,
                RequestHandler::noOp);
        requestDispatcher.registerRequestHandler("login",
                userAuthentication,
                Principal.class,
                UserAuthentication::authenticateUser,
                RequestHandler::noOp,
                handler -> {
            Principal principal = (Principal) handler.result.get();
            if (principal.isAuthenticated()) {
                Session.getSession().setPrincipal(principal);
            }
            return null;
                });
        requestDispatcher.registerRequestHandler("message",
                messaging,
                Message.class,
                Messaging::send,
                messageRequestHandler -> {
            messageRequestHandler.checkAuthenticated();
            Message message = messageRequestHandler.argument.get();
            String username = Session.getSession().getPrincipal().getLogin();
            message.setAuthor(username);
            messageRequestHandler.argument.set(message);
            return null;
                },
                elem->null);
        requestDispatcher.registerRequestHandler("listConversations",
                conversationsManagement,
                EmptyDto.class,
                (conversationsManagement, emptyDto) -> {
            String username = Session.getSession().getPrincipal().getLogin();
            return conversationsManagement.ofUser(username);
                },
                RequestHandler::checkAuthenticated,
                RequestHandler::noOp);
        requestDispatcher.registerRequestHandler("createConversation",
                conversationsManagement,
                Conversation.class,
                (conversationsManagement, conversation) -> {
                    String username = Session.getSession().getPrincipal().getLogin();
                    conversation.getUsers().add(username);
                    return conversationsManagement.createConversation(conversation);
                },
                RequestHandler::checkAuthenticated,
                RequestHandler::noOp);
        requestDispatcher.registerRequestHandler("getMessages",
                messaging,
                EmptyDto.class,
                (messaging, emptyDto) -> {
                    String username = Session.getSession().getPrincipal().getLogin();
                    return messaging.getUserNewMessages(username);
                },
                RequestHandler::checkAuthenticated,
                RequestHandler::noOp);
        requestDispatcher.registerRequestHandler("getConversationMessages",
                messaging,
                GetMessagesDto.class,
                (messaging, getMessagesDto) -> {
                    String username = Session.getSession().getPrincipal().getLogin();
                    return messaging.getConversationMessages(getMessagesDto.conversation, username);
                },
                RequestHandler::checkAuthenticated,
                RequestHandler::noOp);
        return requestDispatcher;
    }

    @Bean
    ListeningSocketServer listeningSocketServer(RequestDispatcher requestDispatcher) {
        return new ListeningSocketServer(port, requestDispatcher);
    }
}
