package pl.edu.pwr.bsiui.server.persistence;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.io.Serializable;
import java.time.OffsetDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@Table(schema = "bsiui", name = "MEMBERSHIPS")
class MembershipEntity {

    @EmbeddedId
    MembershipId id;

    @ManyToOne
    @MapsId("userId")
    UserEntity user;

    @ManyToOne
    @MapsId("conversationId")
    ConversationEntity conversation;

    OffsetDateTime lastSeen;

    @Data
    @Embeddable
    @AllArgsConstructor
    @NoArgsConstructor
    static class MembershipId implements Serializable {
        Long userId;
        Long conversationId;
    }
}
