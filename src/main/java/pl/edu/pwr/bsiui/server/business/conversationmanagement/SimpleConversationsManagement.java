package pl.edu.pwr.bsiui.server.business.conversationmanagement;

import lombok.RequiredArgsConstructor;
import org.springframework.transaction.annotation.Transactional;
import pl.edu.pwr.bsiui.server.business.model.Conversation;
import pl.edu.pwr.bsiui.server.business.persistence.Conversations;
import pl.edu.pwr.bsiui.server.business.persistence.Memberships;

import java.util.Set;

@RequiredArgsConstructor
class SimpleConversationsManagement implements ConversationsManagement {

    private final Conversations conversations;
    private final Memberships memberships;

    @Override
    @Transactional
    public Conversation createConversation(Conversation conversation) {
        conversations.addNamed(conversation.getName());
        conversation.getUsers().forEach(username ->{
            memberships.addUserTo(username, conversation.getName());
        });
        return conversation;
    }

    @Override
    public Set<Conversation> ofUser(String username) {
        return conversations.of(username);
    }

    @Override
    public Set<Conversation> allConversations() {
        return conversations.all();
    }
}
