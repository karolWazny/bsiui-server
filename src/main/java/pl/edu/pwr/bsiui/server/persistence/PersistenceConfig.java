package pl.edu.pwr.bsiui.server.persistence;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import pl.edu.pwr.bsiui.server.business.persistence.Conversations;
import pl.edu.pwr.bsiui.server.business.persistence.Memberships;
import pl.edu.pwr.bsiui.server.business.persistence.Messages;
import pl.edu.pwr.bsiui.server.business.persistence.Users;

@Configuration
@PropertySource("classpath:persistence.properties")
class PersistenceConfig {

    @Bean
    public Users users(UserRepository userRepository,
                       UserMapper userMapper) {
        return new HibernateUsers(userRepository, userMapper);
    }

    @Bean
    public Conversations conversations(ConversationsRepository conversationsRepository,
                                       ConversationMapper conversationMapper){
        return new HibernateConversations(conversationsRepository, conversationMapper);
    }

    @Bean
    public Memberships memberships(MembershipsRepository membershipsRepository,
                                   MembershipMapper membershipMapper,
                                   DateTimeMapper dateTimeMapper,
                                   UserRepository userRepository,
                                   ConversationsRepository conversationsRepository) {
        return new HibernateMemberships(membershipsRepository, membershipMapper, dateTimeMapper, userRepository, conversationsRepository);
    }

    @Bean
    Messages messages(MessageRepository messageRepository,
                      MessageMapper messageMapper,
                      DateTimeMapper dateTimeMapper,
                      ConversationsRepository conversationsRepository,
                      UserRepository userRepository) {
        return new HibernateMessages(messageRepository, messageMapper, dateTimeMapper, conversationsRepository, userRepository);
    }
}
