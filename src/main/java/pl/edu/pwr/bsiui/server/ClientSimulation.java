package pl.edu.pwr.bsiui.server;

import com.google.common.hash.Hashing;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.edu.pwr.bsiui.server.ciphering.AES;
import pl.edu.pwr.bsiui.server.ciphering.Ciphering;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Random;


public class ClientSimulation {
    public static void clientTest() throws Exception {
        Socket clientSocket = new Socket("127.0.0.1", 8091);
        PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
        BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        out.println("{\"action\":\"register\",\"body\":{\"login\":\"log\",\"password\":\"pass\"}}");
        var ret = in.readLine();
        System.out.println(ret);
        clientSocket.close();
    }

    public static void clientFromBois() throws Exception {
        Logger LOGGER = LoggerFactory.getLogger(ClientSimulation.class);
        //generate client keys
        KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA");
        kpg.initialize(1024);
        KeyPair kp = kpg.generateKeyPair();
        PublicKey clientPublicKey = kp.getPublic();
        PrivateKey clientPrivateKey = kp.getPrivate();
        String clientPublicKeyString = Base64.getEncoder().encodeToString(clientPublicKey.getEncoded());

        // connect to server
        Socket clientSocket = new Socket("127.0.0.1", 16123);
        PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
        BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        out.println("Hi!");

        int currentPort = Integer.parseInt(in.readLine());
        System.out.println("port =" + currentPort);
        clientSocket.close();

        // connection on new port
        Socket connectedSocket = new Socket("127.0.0.1", currentPort);
        out = new PrintWriter(connectedSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(connectedSocket.getInputStream()));

        // send user public key
        out.println(clientPublicKeyString);

        // get encoded server key
        String serverPublicKeyMessage = in.readLine();
        String decodedServerKey = Ciphering.decryptHugeText(serverPublicKeyMessage, clientPrivateKey);
        LOGGER.info("SERVER PUBLIC KEY = " + decodedServerKey);

        // get server public key as object
        KeyFactory keyFactory = KeyFactory.getInstance("RSA");
        X509EncodedKeySpec keySpecX509 = new X509EncodedKeySpec(Base64.getDecoder().decode(decodedServerKey));
        PublicKey serverPublicKey = keyFactory.generatePublic(keySpecX509);

        // send control message
        String controlMessage = controlMessageGenerator(100);
        LOGGER.info("CONTROL MESSAGE = " + controlMessage);
        String encodedControlMessage = Ciphering.encryptHugeText(controlMessage, serverPublicKey);
        LOGGER.info("ENCODED CONTROL MESSAGE = " + encodedControlMessage);
        out.println(encodedControlMessage);

        // get control message hash from server
        String controlHashFromServer = in.readLine();
        LOGGER.info("CONTROL HASH = " + controlHashFromServer);

        // decode hash
        String decodedHashFromServer = Ciphering.decryptHugeText(controlHashFromServer, clientPrivateKey);
        LOGGER.info("ENCODED CONTROL HASH = " + decodedHashFromServer);

        // hash control message in client
        String controlHashTest = Hashing
                .sha256()
                .hashString(controlMessage + clientPublicKeyString, StandardCharsets.UTF_8)
                .toString();
        LOGGER.info("CLIENT CODED HASH = " + controlHashTest);

        // check is hash the same
        if (controlHashTest.equals(decodedHashFromServer)) {
            LOGGER.info("SERVER VERIFIED");

        } else {
            LOGGER.info("SERVER IS NOT TRUSTED");
        }

        // send encrypted AES

        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
        SecretKey key = keyGenerator.generateKey();
        String encodedKey = Base64.getEncoder().encodeToString(key.getEncoded());
        LOGGER.info("CLIENT AES: " + encodedKey);
        out.println(Ciphering.encryptHugeText(encodedKey, serverPublicKey));


        String message = "{\"action\": \"login\", \"body\":{\"login\":\"login2\",\"password\":\"haslo2\"}}";

        String encrypted = AES.encrypt(message, encodedKey);
        String decrypted = AES.decrypt(encrypted, encodedKey);
        out.println(encrypted);

        String response = in.readLine();
        LOGGER.info(response);


    }

    private static String controlMessageGenerator(int length) {
        byte[] array = new byte[length];
        new Random().nextBytes(array);
        return Base64.getEncoder().encodeToString(array);
    }

}
