package pl.edu.pwr.bsiui.server.persistence;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@Table(schema = "bsiui", name = "CONVERSATIONS")
class ConversationEntity {
    @Id
    @SequenceGenerator(name = "CONVERSATIONS_GENERATOR", sequenceName = "bsiui.CONVERSATIONS_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CONVERSATIONS_GENERATOR")
    Long id;

    @Column(unique = true)
    String name;

    @OneToMany(mappedBy = "conversation")
    List<MembershipEntity> memberships;
}
