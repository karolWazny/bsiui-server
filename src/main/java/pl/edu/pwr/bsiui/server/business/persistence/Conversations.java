package pl.edu.pwr.bsiui.server.business.persistence;

import pl.edu.pwr.bsiui.server.business.model.Conversation;

import java.util.Set;

public interface Conversations {
    Set<Conversation> all();

    void addNamed(String name);

    Set<Conversation> of(String username);
}
