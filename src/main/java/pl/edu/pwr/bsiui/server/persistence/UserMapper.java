package pl.edu.pwr.bsiui.server.persistence;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import pl.edu.pwr.bsiui.server.business.model.User;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
interface UserMapper {
    UserEntity userToUserEntity(User user);
    User entityToUser(UserEntity user);
}
