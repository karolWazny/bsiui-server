package pl.edu.pwr.bsiui.server.business.userauthentication;

import pl.edu.pwr.bsiui.server.business.model.Principal;

public interface UserAuthentication {
    Principal authenticateUser(Principal unauthenticated);
}
