package pl.edu.pwr.bsiui.server.business.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDateTime;

@Builder
@Getter
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@ToString
public class Membership {
    String username;
    String conversationName;
    LocalDateTime lastSeen;
}
