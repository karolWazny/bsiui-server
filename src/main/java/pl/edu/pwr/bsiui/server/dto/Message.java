package pl.edu.pwr.bsiui.server.dto;

public class Message {
    public String author;
    public String dateTime;
    public String content;
}
