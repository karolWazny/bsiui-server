package pl.edu.pwr.bsiui.server.ciphering;

import java.security.*;

public class ServerKeyStore {

    private PrivateKey serverPrivate;
    private PublicKey serverPublic;
    static ServerKeyStore store;

    public PrivateKey getServerPrivate() {
        return serverPrivate;
    }

    public PublicKey getServerPublic() {
        return serverPublic;
    }

    private ServerKeyStore(int keySize, String algorithm) {

        try {
            KeyPairGenerator keyGen = KeyPairGenerator.getInstance(algorithm);
            keyGen.initialize(keySize);
            KeyPair pair = keyGen.generateKeyPair();

            this.serverPrivate = pair.getPrivate();
            this.serverPublic = pair.getPublic();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    public static ServerKeyStore build(int keySize, String algorithm) {
        if (ServerKeyStore.store == null)
            ServerKeyStore.store = new ServerKeyStore(keySize, algorithm);
        return ServerKeyStore.store;
    }

    public static ServerKeyStore getInstance() {
        return ServerKeyStore.store;
    }

}
