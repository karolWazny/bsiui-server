package pl.edu.pwr.bsiui.server;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import pl.edu.pwr.bsiui.server.business.conversationmanagement.ConversationsManagement;
import pl.edu.pwr.bsiui.server.business.messaging.Messaging;
import pl.edu.pwr.bsiui.server.business.model.Conversation;
import pl.edu.pwr.bsiui.server.business.model.Message;
import pl.edu.pwr.bsiui.server.ciphering.ServerKeyStore;
import pl.edu.pwr.bsiui.server.connection.ListeningSocketServer;
import pl.edu.pwr.bsiui.server.business.model.User;
import pl.edu.pwr.bsiui.server.business.usermanagement.UserManagement;

import java.util.Set;

@SpringBootApplication
@ComponentScan("pl.edu.pwr.bsiui")
@PropertySource("classpath:application.properties")
@PropertySource("classpath:bsiui.properties")
public class Main {

    public static void main(String[] args) throws InterruptedException {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.scan("pl.edu.pwr.bsiui");
        context.refresh();
        UserManagement userManagement = context.getBean(UserManagement.class);

        //prepareTestUsers
        User user = new User("some-login", "secretPassword123");
        userManagement.registerUser(user);
        User user2 = new User("other-login", "topTopSecret!@#");
        userManagement.registerUser(user2);

        ConversationsManagement conversationsManagement = context.getBean(ConversationsManagement.class);

        //create conversation
        Conversation conversation1 = Conversation.builder()
                .name("my-first-convo")
                .users(Set.of("some-login", "other-login"))
                .build();
        conversationsManagement.createConversation(conversation1);
        Conversation conversation2 = Conversation.builder()
                .name("my-second-convo")
                .users(Set.of("some-login"))
                .build();
        conversationsManagement.createConversation(conversation2);
        Conversation conversation3 = Conversation.builder()
                .name("my-third-convo")
                .users(Set.of("other-login"))
                .build();
        conversationsManagement.createConversation(conversation3);

        System.out.println("[TEST]" + conversationsManagement.ofUser("some-login"));
        System.out.println("[TEST]" + conversationsManagement.allConversations());

        Thread.sleep(500);

        Messaging messaging = context.getBean(Messaging.class);
        System.out.println("[TEST][SHOULD BE EMPTY]" + messaging.getUserNewMessages("other-login"));
        Thread.sleep(500);
        messaging.send(Message.builder()
                        .conversation("my-first-convo")
                        .author("some-login")
                        .content("Lorem ipsum")
                .build());
        messaging.send(Message.builder()
                        .conversation("my-first-convo")
                        .author("some-login")
                        .content("sit dolor")
                .build());
        messaging.send(Message.builder()
                        .conversation("my-first-convo")
                        .author("some-login")
                        .content("post scriptum")
                .build());
        Thread.sleep(500);
        System.out.println("[TEST][SHOULD NOT BE EMPTY]" + messaging.getUserNewMessages("other-login"));
        System.out.println("[TEST][SHOULD NOT BE EMPTY]" + messaging.getConversationMessages("my-first-convo",
                "other-login"));
        System.out.println("[TEST][SHOULD BE EMPTY]" + messaging.getUserNewMessages("other-login"));


        try {
            ServerKeyStore.build(1024, "RSA");
            var listeningSocketServer = context.getBean(ListeningSocketServer.class);
            listeningSocketServer.start();

            //ClientSimulation.clientTest();
            ClientSimulation.clientFromBois();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
