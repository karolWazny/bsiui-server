package pl.edu.pwr.bsiui.bsiui.server;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import pl.edu.pwr.bsiui.server.Main;

@SpringBootTest(classes = Main.class)
class MainTests {

    @Test
    void contextLoads() {
    }

}
